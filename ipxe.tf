provider "digitalocean" {
  token = "${var.digitalocean_token}"
}

resource "digitalocean_droplet" "salt_master" {
  name = "ipxe-service-salt"
  image = "ubuntu-16-04-x64"
  size  = "${var.size}"
  ssh_keys = ["${var.ssh_key_fingerprint}"]
  connection {
    user = "root"
    private_key = "${file("${var.priv_ssh_key_path}")}"
  }
  provisioner "file" {
    source = "master.sh"
    destination = "/root/master.sh"
  }
  provisioner "local-exec" {
    command = "sed -e 's|SALT_MASTER|${self.ipv4_address}|' minion-template.sh > tmp/minion.sh"
  }
  provisioner "remote-exec" {
    inline = "sh /root/master.sh"
  }
  user_data = "#cloud-config\n\nssh_authorized_keys:\n  - \"${file("${var.ssh_public_key_path}")}\""
  region = "${var.region}"
}

resource "digitalocean_droplet" "ipxe_host" {
  depends_on = ["digitalocean_droplet.salt_master"]  
  name = "${format("ipxe-service-host-%02d", count.index)}"
  image = "ubuntu-16-04-x64"
  count = "${var.count}"
  size  = "${var.size}"
  ssh_keys = ["${var.ssh_key_fingerprint}"]
  connection {
    user = "root"
    private_key = "${file("${var.priv_ssh_key_path}")}"
  }
  provisioner "file" {
    source = "tmp/minion.sh"
    destination = "/root/minion.sh"
  }
  provisioner "remote-exec" {
    inline = "sh /root/minion.sh"
  }
  user_data = "#cloud-config\n\nssh_authorized_keys:\n  - \"${file("${var.ssh_public_key_path}")}\""
  region = "${var.region}"
}

resource "digitalocean_loadbalancer" "public" {
  depends_on = ["digitalocean_droplet.ipxe_host"]
  name = "ipxe-proxy.${var.region}"
  region = "${var.region}"
  algorithm = "least_connections"
  forwarding_rule {
    entry_port = 80
    entry_protocol = "http"

    target_port = 80
    target_protocol = "http"
  }
  healthcheck {
    port = 22
    protocol = "tcp"
  }
}


