#!/bin/bash

apt-get update && \
apt-get install salt-master -y && \
echo "" > /etc/salt/master && \
echo "id: `hostname`" >> /etc/salt/master && \
service salt-master restart 