#!/bin/bash

apt-get update && \
apt-get install salt-minion salt-common -y && \
echo "" > /etc/salt/minion && \
echo "id: `hostname`" >> /etc/salt/minion && \
echo "master: SALT_MASTER" >> /etc/salt/minion && \
service salt-minion restart