output "Your hosts" {
  value = "${join(",", digitalocean_droplet.ipxe_host.*.ipv4_address)}"
}

output "Message" {
  value = "You'll need to login to ${digitalocean_droplet.salt_master.ipv4_address} and accept the Salt keys before proceeding further."
}

output "Next Steps" {
  value = "Go to https://cloud.digitalocean.com/networking/load_balancers to make any changes to your Load Balancer."
}

